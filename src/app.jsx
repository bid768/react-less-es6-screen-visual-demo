import React from 'react'
import ReactDOM from 'react-dom'
import { Router, Route } from 'react-router-dom'

// import moment from 'moment';
// import 'moment/locale/zh-cn';

import HomePage from 'modules/index'
import 'styles/base.css'

// 引入组件
import EchartsFamilyGraph from 'modules/echartsFamilyGraph/echartsFamilyGraph.jsx'
import EchartsLineMap from 'modules/echartsLineMap/echartsLineMap.jsx'

const createHistory = require('history').createBrowserHistory

class App extends React.Component {
    render() {
        return (
            <Router history={createHistory()}>
                <Route
                    render={() => {
                        return (
                            <HomePage>
                                <Route
                                    path="/"
                                    exact
                                    component={EchartsFamilyGraph}
                                />

                                <Route
                                    path="/echartsFamilyGraph"
                                    component={EchartsFamilyGraph}
                                />
                                <Route
                                    path="/echartsLineMap"
                                    component={EchartsLineMap}
                                />
                            </HomePage>
                        )
                    }}
                ></Route>
            </Router>
        )
    }
}

ReactDOM.render(<App />, document.getElementById('root'))
