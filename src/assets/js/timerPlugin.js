class TimerPlugin {
    /**
     * 添加定时器
     *
     * @static
     * @param {function} fCallback 回调函数
     * @param {number} interval 间隔时长
     * @param {number} [loopCount=0] 循环次数
     * @param {any} fParam 回调函数入参
     * @returns 定时器对象
     * @memberof TimerPlugin
     */
    static addTimer(fCallback, interval, loopCount = 0, ...fParam) {
        let start = null
        const timerObj = { pause: false }
        let loopNum = 0

        function step(timestamp) {
            if (!start) start = timestamp
            const progress = timestamp - start
            if (progress >= interval && !timerObj.pause) {
                fCallback(fParam)
                ++loopNum
                if (loopNum === loopCount) {
                    TimerPlugin.deleteTimer(timerObj)
                }
                start = null
            }
            window.requestAnimationFrame(step)
        }
        timerObj.id = window.requestAnimationFrame(step)
        return timerObj
    }

    // 暂停定时器
    static pauseTimer(timerObj) {
        timerObj.pause = true
    }

    // 重启定时器
    static reStartTimer(timerObj) {
        timerObj.pause = false
    }

    // 删除定时器
    static deleteTimer(timerObj) {
        if (timerObj) {
            window.cancelAnimationFrame(timerObj.id)
            timerObj.id = 0
        }
    }
}
export default TimerPlugin
