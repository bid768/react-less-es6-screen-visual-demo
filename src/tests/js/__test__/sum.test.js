test('adds 1 + 2 to equal 3 in es', () => {
    const sum = require('../sum.js').default
    expect(sum(1, 2)).toBe(3)
})
