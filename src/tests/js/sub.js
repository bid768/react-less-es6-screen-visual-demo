import sum from './sum'

function sub(a, b) {
    return sum(a, -b)
}

export default sub
